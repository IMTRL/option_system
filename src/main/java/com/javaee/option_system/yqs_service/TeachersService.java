package com.javaee.option_system.yqs_service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javaee.option_system.entity.PageResult;
import com.javaee.option_system.entity.Term;
import com.javaee.option_system.mapper.yqs_mapper.CoursesArrangementMapper;
import com.javaee.option_system.mapper.yqs_mapper.CoursesInfoMapper;
import com.javaee.option_system.mapper.yqs_mapper.CoursesSelectionMapper;
import com.javaee.option_system.mapper.yqs_mapper.TeacherMapper;
import com.javaee.option_system.pojo.Teachers;
import com.javaee.option_system.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.io.File;
import java.util.List;
import java.util.Map;

@Service
public class TeachersService {
    @Value("${myconfig.filepath}")
    private File systemSetting;
    @Resource
    private JsonUtils jsonUtils;
    @Resource
    private TeacherMapper teacherMapper;
    @Resource
    private CoursesSelectionMapper coursesSelectionMapper;
    @Resource
    private CoursesInfoMapper coursesInfoMapper;
    @Resource
    private CoursesArrangementMapper coursesArrangementMapper;

    public List<Teachers> selectAllTea(){
        return teacherMapper.selectAll();
    }


    public String update_Tea(Map<String, Object> map){
        teacherMapper.updateTea(String.valueOf(map.get("teacherName")),String.valueOf(map.get("teacherSex")),Integer.parseInt(map.get("teacherAge").toString()),String.valueOf(map.get("teacherPhone")),String.valueOf(map.get("teacherNo")));
        return "教师信息更新成功";
    }

    public String setStudentScore(Map<String, Object> map){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        coursesSelectionMapper.setScore(term.getYear(),term.getTerm(),String.valueOf(map.get("studentNumber")),Double.parseDouble(map.get("studentScore").toString()),String.valueOf(map.get("courseNumber")));
        return "设置分数成功";
    }

     public List<Map<String,Object>> stuCourseSelection(Map<String, Object> map){
         Term term=jsonUtils.readJson(systemSetting,Term.class);
        return coursesSelectionMapper.stuCourseSelection(term.getYear(), term.getTerm(),String.valueOf(map.get("teacherNumber")));
    }

    public String addCourse(Map<String, Object> map){
        Boolean bol=String.valueOf(map.get("courseMultimedia")).equals("true");
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        coursesInfoMapper.addCourse(term.getYear(), term.getTerm(),String.valueOf(map.get("courseNo")),String.valueOf(map.get("teacherNumber")),String.valueOf(map.get("courseName")),String.valueOf(map.get("courseObject")),Integer.parseInt(map.get("coursePeople").toString()),bol,Integer.parseInt(map.get("courseCredit").toString()));
        coursesArrangementMapper.addCourseArr(term.getYear(), term.getTerm(),String.valueOf(map.get("courseNo")),String.valueOf(map.get("courseObject")));
        return "添加课程成功";
    }

    public List<Map<String,Object>> teacherCourse(Map<String, Object> map){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        return coursesInfoMapper.teacherCourse(term.getYear(), term.getTerm(),String.valueOf(map.get("teacherNo")));
    }

    public Teachers teacherInfo(Map<String, Object> map){
        return teacherMapper.teacherInfo(String.valueOf(map.get("teacherNo")));
    }

    public String updateTeaPassword(Map<String, Object> map){
        teacherMapper.updatePassword(String.valueOf(map.get("teacherPassword")),String.valueOf(map.get("teacherNewPassword")),String.valueOf(map.get("teacherNo")));
        return "教师密码修改成功";
    }

    public List<Map<String,Object>> teaCourseArrangement(Map<String, Object> map){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        return coursesArrangementMapper.teaCourseArrangement(term.getYear(), term.getTerm(),String.valueOf(map.get("teacherNumber")));
    }


    public List<Map<String,Object>> studentCourseList(Map<String, Object> map){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        return coursesSelectionMapper.studentCourseList(term.getYear(), term.getTerm(),String.valueOf(map.get("teacherNo")),String.valueOf(map.get("courseNo")));
    }
}
