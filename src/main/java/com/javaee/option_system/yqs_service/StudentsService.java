package com.javaee.option_system.yqs_service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javaee.option_system.entity.PageResult;
import com.javaee.option_system.entity.Term;
import com.javaee.option_system.mapper.yqs_mapper.CoursesArrangementMapper;
import com.javaee.option_system.mapper.yqs_mapper.CoursesInfoMapper;
import com.javaee.option_system.mapper.yqs_mapper.CoursesSelectionMapper;
import com.javaee.option_system.mapper.yqs_mapper.StudentMapper;
import com.javaee.option_system.pojo.CourseSelection;
import com.javaee.option_system.pojo.Students;
import com.javaee.option_system.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.io.File;
import java.util.List;
import java.util.Map;

@Service
public class StudentsService {
    @Value("${myconfig.filepath}")
    private File systemSetting;
    @Resource
    private JsonUtils jsonUtils;
    @Resource
    private StudentMapper studentMapper;
    @Resource
    private CoursesArrangementMapper coursesArrangementMapper;
    @Resource
    private CoursesSelectionMapper coursesSelectionMapper;
    @Resource
    private CoursesInfoMapper coursesInfoMapper;

    public String update_Stu(Map<String, Object> map){
         studentMapper.updateStu(String.valueOf(map.get("studentName")),String.valueOf(map.get("studentSex")),Integer.parseInt(map.get("studentAge").toString()),String.valueOf(map.get("studentNo")));
        return "学生信息更新成功";
    }

    public List<Map<String,Object>> getCourseArrangement(Map<String, Object> map){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        return coursesArrangementMapper.getCourseArrangement(term.getYear(), term.getTerm(),String.valueOf(map.get("studentNumber")));
    }

    public String chooseCourses(Map<String, Object> map){
        Term term=jsonUtils.readJson(systemSetting,Term.class);

        coursesSelectionMapper.chooseCourse(term.getYear(), term.getTerm(),String.valueOf(map.get("studentNumber")),String.valueOf(map.get("courseNumber")));
        return "选课成功";
    }

    public List<Map<String,Object>> selectScore(Map<String, Object> map){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        return coursesSelectionMapper.selectScore(term.getYear(), term.getTerm(),String.valueOf(map.get("studentNo")));
    }

    public Students studentInfo(Map<String, Object> map){
        return studentMapper.studentInfo(String.valueOf(map.get("studentNo")));
    }

    public String deleteCourseSelection(Map<String, Object> map){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        coursesSelectionMapper.deleteCourseSelection(term.getYear(), term.getTerm(),String.valueOf(map.get("studentNumber")),String.valueOf(map.get("courseNumber")));
        return "退选成功";
    }

    public List<Map<String,Object>> courseInfo(){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        return coursesInfoMapper.courseInfo(term.getYear(), term.getTerm());
    }

    public String updateStuPassword(Map<String, Object> map){
        studentMapper.updatePassword(String.valueOf(map.get("studentPassword")),String.valueOf(map.get("studentNewPassword")),String.valueOf(map.get("studentNo")));
        return "学生密码修改成功";
    }

    public List<Map<String,Object>> studentClassSelect(Map<String, Object> map){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        return coursesSelectionMapper.studentClassSelect(term.getYear(), term.getTerm(),String.valueOf(map.get("studentNo")));
    }
}
