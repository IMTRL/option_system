package com.javaee.option_system.controller;


import com.alibaba.excel.EasyExcel;
import com.javaee.option_system.entity.ReturnInfo;
import com.javaee.option_system.enums.StatusCode;
import com.javaee.option_system.listener.ClassroomListener;
import com.javaee.option_system.listener.StudentListener;
import com.javaee.option_system.listener.TeacherListener;
import com.javaee.option_system.pojo.Classroom;
import com.javaee.option_system.pojo.Students;
import com.javaee.option_system.pojo.Teachers;
import com.javaee.option_system.service.ClassroomService;
import com.javaee.option_system.service.StudentService;
import com.javaee.option_system.service.TeacherService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;

@RestController
public class ExcelController {
    @Resource
    private StudentService studentService;
    @Resource
    private TeacherService teacherService;
    @Resource
    private ClassroomService classroomService;



    @PostMapping("/manager/upload/student")
    public ReturnInfo uploadStudent(MultipartFile file) throws IOException {
        EasyExcel.read(file.getInputStream(), Students.class, new StudentListener(studentService)).sheet().doRead();
        return new ReturnInfo(StatusCode.OK,"上传成功","student");
    }
    @PostMapping("/manager/upload/teacher")
    public ReturnInfo uploadTeacher(MultipartFile file) throws IOException {
        EasyExcel.read(file.getInputStream(), Teachers.class, new TeacherListener(teacherService)).sheet().doRead();
        return new ReturnInfo(StatusCode.OK,"上传成功","teacher");
    }
    @PostMapping("/manager/upload/classroom")
    public ReturnInfo uploadClassroom(MultipartFile file) throws IOException {
        EasyExcel.read(file.getInputStream(), Classroom.class, new ClassroomListener(classroomService)).sheet().doRead();
        return new ReturnInfo(StatusCode.OK,"上传成功","classroom");
    }

}
