package com.javaee.option_system.controller;

import com.javaee.option_system.entity.ReturnInfo;
import com.javaee.option_system.enums.StatusCode;
import com.javaee.option_system.service.ManagerService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
public class ManagerController {
    @Resource
    private ManagerService managerService;

    @GetMapping("/manager/allot")
    public ReturnInfo setClassroom(){
        return new ReturnInfo(StatusCode.OK,"教室分配完成", managerService.setClassroom());
    }

    @PostMapping("/manager/fenpei")
    public ReturnInfo fenpeiClassroom(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"分配结束", managerService.fenpeiAganin(map));
    }

    @GetMapping("/manager/classroom/select")
    public ReturnInfo selectAllClassroom(@RequestParam Map map){
        return new ReturnInfo(StatusCode.OK,"所有教室查询完成", managerService.managerSelectAllClassroom(map));
    }

    @GetMapping("/manager/student/select")
    public ReturnInfo selectAllStu(@RequestParam Map map){
        return new ReturnInfo(StatusCode.OK,"所有学生查询完成", managerService.managerSelectAllStu(map));
    }

    @GetMapping("/manager/teacher/select")
    public ReturnInfo selectAllTea(@RequestParam Map map){
        return new ReturnInfo(StatusCode.OK,"所有教师查询完成", managerService.managerSelectAllTea(map));
    }

    @PostMapping("/manager/teacher/add")
    public ReturnInfo addTeacher(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"教师添加成功", managerService.managerAddTeacher(map));
    }

    @PostMapping("/manager/student/add")
    public ReturnInfo addStudent(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"学生添加成功", managerService.managerAddStudent(map));
    }

    @PostMapping("/manager/classroom/add")
    public ReturnInfo addClassroom(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"教室添加成功", managerService.managerAddClassroom(map));
    }

    @PostMapping("/manager/student/reset")
    public ReturnInfo resetStuPassword(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"重置学生密码查询完成", managerService.resetStuPassword(map));
    }

    @PostMapping("/manager/student/delete")
    public ReturnInfo delStu(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"删除学生完成", managerService.delStu(map));
    }
    @PostMapping("/manager/teacher/delete")
    public ReturnInfo delTea(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"删除教师完成", managerService.delTea(map));
    }
    @PostMapping("/manager/classroom/delete")
    public ReturnInfo delClassroom(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"删除教室完成", managerService.delClassroom(map));
    }

    @GetMapping("/manager/select")
    public ReturnInfo selectManager(HttpServletRequest request){
        return new ReturnInfo(StatusCode.OK,"管理员信息查询完成", managerService.selectManager(request));
    }

    @PostMapping("/manager/edit")
    public ReturnInfo setManagerInfo(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"管理员信息修改完成", managerService.setManagerInfo(map));
    }

    @PostMapping("/manager/classroom/edit")
    public ReturnInfo setClassroomInfo(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"教室信息修改完成", managerService.setClassroomInfo(map));
    }

    @PostMapping("/manager/new")
    public ReturnInfo openNewTerm(@RequestBody Map<String, Object> map){
        return new ReturnInfo(StatusCode.OK,"新学期开始", managerService.openNewTerm(map));
    }
}
