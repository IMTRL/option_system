package com.javaee.option_system.controller;

import com.javaee.option_system.entity.ReturnInfo;
import com.javaee.option_system.enums.ExceptionEnums;
import com.javaee.option_system.enums.StatusCode;
import com.javaee.option_system.exception.MyException;
import com.javaee.option_system.service.LoginService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Map;

@RestController
public class LoginController {
    @Resource
    private LoginService loginService;


    @PostMapping("/login")
    public ReturnInfo loginManager(@RequestBody Map<String, Object> map) {
        if ("manager".equals(map.get("role")))
            return new ReturnInfo(StatusCode.OK, "管理员登陆成功", loginService.mangerLogin(map));
        if ("student".equals(map.get("role")))
            return new ReturnInfo(StatusCode.OK, "学生登陆成功", loginService.studentLogin(map));
        if ("teacher".equals(map.get("role")))
            return new ReturnInfo(StatusCode.OK, "教师登陆成功", loginService.teacherLogin(map));
        return new ReturnInfo(StatusCode.ERROR, "权限信息有误", "请联系管理员");
    }

}
