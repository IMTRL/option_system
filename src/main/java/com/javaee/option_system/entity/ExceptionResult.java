package com.javaee.option_system.entity;

import lombok.Data;
import com.javaee.option_system.enums.ExceptionEnums;

@Data
public class ExceptionResult {
    private int status ;
    private String msg ;
    private Long timestamp;

    public ExceptionResult(ExceptionEnums em ) {
        this.status=em.getCode();
        this.msg = em.getMsg();
        this.timestamp = System.currentTimeMillis();
    }
}
