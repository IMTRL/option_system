package com.javaee.option_system.entity;

import com.javaee.option_system.pojo.CourseArrangement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SetClassroomResult {
    private Map<String, List<CourseArrangement>> result;
    private SetClassroomSurplus surplus;
}
