package com.javaee.option_system.entity;

import com.javaee.option_system.pojo.CourseArrangement;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SetClassroomSurplus {
    private String message;
    private Map<String,Integer> classroom;
    private List<CourseArrangement> courseArrangement;
}
