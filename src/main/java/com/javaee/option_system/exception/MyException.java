package com.javaee.option_system.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import com.javaee.option_system.enums.ExceptionEnums;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class MyException extends RuntimeException {
    private ExceptionEnums exceptionEnums;
}
