package com.javaee.option_system.enums;

public class StatusCode {
    public static final int OK = 200;     //成功
    public static final int ERROR = 400;     //失败
    public static final int NO_CONTENT = 204;     //操作成功
    public static final int CREATE = 201;     //创建成功

}
