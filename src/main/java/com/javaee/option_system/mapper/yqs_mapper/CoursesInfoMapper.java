package com.javaee.option_system.mapper.yqs_mapper;

import com.javaee.option_system.pojo.CourseInfo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.ConditionMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

import java.util.List;
import java.util.Map;

public interface CoursesInfoMapper extends Mapper<CourseInfo>, MySqlMapper<CourseInfo>, ConditionMapper<CourseInfo>, IdsMapper<CourseInfo> {
    @Select("select * from course_info where course_no=${courseNo}")
    public List<CourseInfo> selectCourseByCourseNo(@Param("courseNo") String courseNo);

    @Insert("insert into ${year}${term}course_info(course_No,teacher_Number,course_Name,course_Object,course_People,course_Multimedia,course_Credit) values(#{courseNo},#{teacherNumber},#{courseName},#{courseObject},#{coursePeople},#{courseMultimedia},#{courseCredit})")
    void addCourse(@Param("year") String year,@Param("term")String term,@Param("courseNo") String courseNo,@Param("teacherNumber") String teacherNumber,@Param("courseName") String courseName,@Param("courseObject") String courseObject,@Param("coursePeople") Integer coursePeople,@Param("courseMultimedia") Boolean courseMultimedia,@Param("courseCredit") Integer courseCredit);

    @Select("select * from ${year}${term}course_info")
    public List<Map<String,Object>> courseInfo(@Param("year") String year,@Param("term")String term);

    @Select("select course_name,teacher_name,course_credit from ${year}${term}course_info,teachers where ${year}${term}course_info.teacher_number=teachers.teacher_no and teachers.teacher_no=#{teacherNo}")
    public List<Map<String,Object>> teacherCourse(@Param("year") String year,@Param("term")String term,@Param("teacherNo") String teacherNo);
}
