package com.javaee.option_system.mapper.yqs_mapper;

import com.javaee.option_system.pojo.Teachers;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.ConditionMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

public interface TeacherMapper extends Mapper<Teachers>, MySqlMapper<Teachers>, ConditionMapper<Teachers>, IdsMapper<Teachers> {
    @Select("select * from teachers where teacher_no = #{userid} and teacher_password = #{password}")
    Teachers login(@Param("userid") String userid, @Param("password") String password);

    @Update("update teachers set teacher_name=#{teacherName},teacher_sex=#{teacherSex},teacher_age=#{teacherAge},teacher_phone=#{teacherPhone} where teacher_no=#{teacherNo}")
    void updateTea(@Param("teacherName") String teacherName, @Param("teacherSex") String teacherSex, @Param("teacherAge") Integer teacherAge, @Param("teacherPhone") String teacherPhone, @Param("teacherNo") String teacherNo);

    @Select("select teacher_no,teacher_name,teacher_sex,teacher_age,teacher_phone from teachers where teacher_no=#{teacherNo}")
    Teachers teacherInfo(@Param("teacherNo") String teacherNo);

    @Update("update teachers set teacher_password=#{teacherNewPassword} where teacher_no=#{teacherNo} and teacher_password=#{teacherPassword}")
    void updatePassword(@Param("teacherPassword") String teacherPassword, @Param("teacherNewPassword") String teacherNewPassword, @Param("teacherNo") String teacherNo);

}
