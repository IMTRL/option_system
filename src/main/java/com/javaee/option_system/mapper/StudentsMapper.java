package com.javaee.option_system.mapper;

import com.javaee.option_system.pojo.Students;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import tk.mybatis.mapper.common.ConditionMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

public interface StudentsMapper extends Mapper<Students>, MySqlMapper<Students>, ConditionMapper<Students>, IdsMapper<Students> {
    @Select("select * from students where student_no = #{userid} and student_password = #{password}")
    Students login(@Param("userid") String userid, @Param("password") String password);

//    @Insert("insert into ${year}students(student_no,student_name,student_sex,student_age) values(#{student_no},#{student_name},#{student_sex},#{student_age})")
//    int addStu(@Param("student_no") String student_no,@Param("student_name")String student_name,@Param("student_sex")String student_sex,@Param("student_age")Integer student_age,@Param("year") String year);


}
