package com.javaee.option_system.mapper;

import com.javaee.option_system.pojo.Teachers;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import tk.mybatis.mapper.common.ConditionMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

import java.util.List;

public interface TeachersMapper extends Mapper<Teachers>, MySqlMapper<Teachers>, ConditionMapper<Teachers>, IdsMapper<Teachers> {
    @Select("select * from teachers where teacher_no = #{userid} and teacher_password = #{password}")
    Teachers login(@Param("userid") String userid, @Param("password") String password);
}
