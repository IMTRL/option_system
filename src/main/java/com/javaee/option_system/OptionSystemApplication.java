package com.javaee.option_system;

import com.javaee.option_system.utils.JwtUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.javaee.option_system.mapper")
public class OptionSystemApplication {

    public static void main(String[] args) {

        SpringApplication.run(OptionSystemApplication.class, args);


    }
    @Bean
    public JwtUtil jwtUtil(){return new JwtUtil();}
}
