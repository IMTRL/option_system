package com.javaee.option_system.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javaee.option_system.entity.PageResult;
import com.javaee.option_system.entity.Term;
import com.javaee.option_system.enums.ExceptionEnums;
import com.javaee.option_system.exception.MyException;
import com.javaee.option_system.mapper.StudentsMapper;
import com.javaee.option_system.pojo.Managers;
import com.javaee.option_system.pojo.Students;
import com.javaee.option_system.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.io.File;
import java.util.List;
import java.util.Map;

@Service
public class StudentService {

    @Resource
    private StudentsMapper studentsMapper;


    public int addStudent(Map<String, Object> map) {
        Students students = new Students(String.valueOf(map.get("student_no")), String.valueOf(map.get("student_name")), String.valueOf(map.get("student_sex")), Integer.parseInt(map.get("student_age").toString()));
        return studentsMapper.insertSelective(students);
    }

    public PageResult<Students> selectAllStu(Map<String, Object> map) {
        Integer pageNum = Integer.valueOf(map.get("page").toString());
        Integer pageSize = Integer.valueOf(map.get("limit").toString());
        PageHelper.startPage(pageNum, pageSize);
        Example example = new Example(Students.class);
        example.excludeProperties("studentPassword");
        List<Students> students = studentsMapper.selectByExample(example);
        PageInfo<Students> userPageInfo = new PageInfo<Students>(students);
        return new PageResult<Students>(userPageInfo.getTotal(), userPageInfo.getList());
    }

    public int resetStuPassword(Map<String, Object> map) {
        Students students = new Students();
        students.setStudentPassword("000000");
        students.setId(Integer.valueOf(map.get("id").toString()));
        int result = studentsMapper.updateByPrimaryKeySelective(students);
        if (result == 0) {
            throw new MyException(ExceptionEnums.TABLE_SET_ERROR);
        }
        return result;
    }

    public int delStu(Map<String, Object> map) {
        int result = studentsMapper.deleteByIds(map.get("id").toString());
        if (result == 0) {
            throw new MyException(ExceptionEnums.TABLE_SET_ERROR);
        }
        return result;
    }


    public void save(List<Students> list) {
        for (Students students : list) {
            students.setStudentPassword("000000");
        }
        try {
            studentsMapper.insertList(list);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ExceptionEnums.TABLE_INFO_ERROR);
        }
    }
}
