package com.javaee.option_system.service;

import com.javaee.option_system.entity.SetClassroomResult;
import com.javaee.option_system.entity.SetClassroomSurplus;
import com.javaee.option_system.entity.Term;
import com.javaee.option_system.enums.ExceptionEnums;
import com.javaee.option_system.exception.MyException;
import com.javaee.option_system.mapper.CourseArrangementMapper;
import com.javaee.option_system.pojo.Classroom;
import com.javaee.option_system.pojo.CourseArrangement;
import com.javaee.option_system.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.io.File;
import java.util.*;

@Service
public class CourseArrangementService {
    @Resource
    private CourseArrangementMapper courseArrangementMapper;
    @Value("${myconfig.filepath}")
    private File systemSetting;
    @Resource
    private JsonUtils jsonUtils;


    /*public List<CourseArrangement> getSetedClassroomByCNum(String classroomNumber) {
        return courseArrangementMapper.getSetedClassroomByCNum(classroomNumber);
    }*/

    public int createTable(Map<String, Object> map){
        String year=String.valueOf(map.get("year"));
        String term=String.valueOf(map.get("term"));
        int result=courseArrangementMapper.createTable(year,term);
        /*if (result==0)
            throw new MyException(ExceptionEnums.TABLE_SET_ERROR);*/
        return result;
    }

    public List<CourseArrangement> getCourseArrangementList() {
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        return courseArrangementMapper.selectAllCourseArrangement(term.getYear(),term.getTerm());
    }

    public SetClassroomResult setClassroomToArrangement(List<Classroom> classroom, List<CourseArrangement> courseArrangement) {
        int classroomLength = classroom.size();
        int courseArrangementLength = courseArrangement.size();
        SetClassroomSurplus setClassroomSurplus = new SetClassroomSurplus();

        if (classroomLength == 0 && courseArrangementLength == 0) {
            setClassroomSurplus.setMessage("null");
            return new SetClassroomResult(null, setClassroomSurplus);
        } else if (classroomLength == 0) {
            setClassroomSurplus.setCourseArrangement(courseArrangement);
            setClassroomSurplus.setMessage("course");
            return new SetClassroomResult(null, setClassroomSurplus);
        } else if (courseArrangementLength == 0) {

            Map<String, Integer> map = new HashMap<>();
            for (int i = 0; i < classroomLength; i++) {
                map.put("" + classroom.get(i).getClassroomNo(), 0);
            }
            setClassroomSurplus.setClassroom(map);
            setClassroomSurplus.setMessage("classroom");
            return new SetClassroomResult(null, setClassroomSurplus);
        }

        Collections.shuffle(classroom);
        Collections.shuffle(courseArrangement);
        Map<String, List<CourseArrangement>> mapca = new HashMap<>();
        for (int i = 0; i < classroomLength; i++) {
            mapca.put("" + classroom.get(i).getClassroomNo(), new LinkedList<CourseArrangement>());
        }
        int CAcounter = courseArrangementLength - 1;
        for (int i = 0; i < 25; i++) {
            for (int j = 0; j < classroomLength; j++) {
                mapca.get("" + classroom.get(j).getClassroomNo()).add(courseArrangement.get(CAcounter));
                CAcounter--;
                if (CAcounter < 0) {
                    Map<String, Integer> map = new HashMap<>();
                    if (j + 1 > 0) {
                        for (int k = 0; k < j + 1; k++) {
                            map.put("" + classroom.get(k).getClassroomNo(), i + 1);
                        }
                    }
                    for (int k = j + 1; k < classroomLength; k++) {
                        map.put("" + classroom.get(k).getClassroomNo(), i);
                    }
                    setClassroomSurplus.setClassroom(map);
                    setClassroomSurplus.setMessage("classroom");
                    break;
                }
            }
            if (CAcounter < 0) break;
        }
        if (CAcounter >= 0) {
            setClassroomSurplus.setCourseArrangement(courseArrangement.subList(0, CAcounter + 1));
            setClassroomSurplus.setMessage("course");
        }
        List<CourseArrangement> courseArrangementList = new LinkedList<>();

        for (String s : mapca.keySet()) {
            List<CourseArrangement>list=mapca.get(s);
            for (int i = 0; i < list.size(); i++) {
                CourseArrangement course=list.get(i);
                course.setClassroomNumber(s);
                course.setCourseTime(i+1);
                courseArrangementList.add(course);
            }
        }
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        int result=courseArrangementMapper.batchUpdate(courseArrangementList,term.getYear(),term.getTerm());
        if (result==0)
            throw new MyException(ExceptionEnums.TABLE_SET_ERROR);
        return new SetClassroomResult(mapca, setClassroomSurplus);

    }

    public int fenpeiAgain(Map<String, Object> map){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        Integer id=Integer.parseInt(map.get("id").toString());
        String classroomNumber=courseArrangementMapper.selectClassroomById(id,term.getYear(),term.getTerm()).getClassroomNumber();
        if (!(classroomNumber==null||classroomNumber.equals("")))
            return 0;
        String classroomNo=String.valueOf(map.get("classroomNo"));
        Integer courseTime=Integer.parseInt(map.get("courseTime").toString());
        return courseArrangementMapper.updateClassroomById(id,classroomNo,courseTime,term.getYear(),term.getTerm());
    }


}
