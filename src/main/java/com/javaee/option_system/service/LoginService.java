package com.javaee.option_system.service;

import com.javaee.option_system.enums.ExceptionEnums;
import com.javaee.option_system.exception.MyException;
import com.javaee.option_system.mapper.ManagersMapper;
import com.javaee.option_system.mapper.StudentsMapper;
import com.javaee.option_system.mapper.TeachersMapper;
import com.javaee.option_system.pojo.Managers;
import com.javaee.option_system.pojo.Students;
import com.javaee.option_system.pojo.Teachers;
import com.javaee.option_system.utils.JwtUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

@Service
public class LoginService {
    @Resource
    private ManagersMapper managersMapper;
    @Resource
    private TeachersMapper teachersMapper;
    @Resource
    private StudentsMapper studentsMapper;

    @Resource
    private JwtUtil jwtUtil;

    public String mangerLogin(Map<String, Object> map) {
        Managers manager =
                managersMapper.login(String.valueOf(map.get("username")), String.valueOf(map.get("password")));
        if (manager==null){
            throw new MyException(ExceptionEnums.USERNAME_OR_PASSWORD_ERROR);
        }else {
            String token=jwtUtil.createJWT(manager.getManagerNo()+"","manager");
            return token;
        }
    }
    public String studentLogin(Map<String, Object> map) {
        Students students =
                studentsMapper.login(String.valueOf(map.get("username")), String.valueOf(map.get("password")));
        if (students==null){
            throw new MyException(ExceptionEnums.USERNAME_OR_PASSWORD_ERROR);
        }else {
            String token=jwtUtil.createJWT(students.getStudentNo()+"","student");
            return token;
        }
    }
    public String teacherLogin(Map<String, Object> map) {
        Teachers teachers =
                teachersMapper.login(String.valueOf(map.get("username")), String.valueOf(map.get("password")));
        if (teachers==null){
            throw new MyException(ExceptionEnums.USERNAME_OR_PASSWORD_ERROR);
        }else {
            String token=jwtUtil.createJWT(teachers.getTeacherNo()+"","teacher");
            return token;
        }
    }

}
