package com.javaee.option_system.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javaee.option_system.entity.PageResult;
import com.javaee.option_system.entity.SetClassroomResult;
import com.javaee.option_system.enums.ExceptionEnums;
import com.javaee.option_system.exception.MyException;
import com.javaee.option_system.mapper.ClassroomMapper;
import com.javaee.option_system.mapper.CourseArrangementMapper;
import com.javaee.option_system.mapper.CourseInfoMapper;
import com.javaee.option_system.mapper.CourseSelectionMapper;
import com.javaee.option_system.pojo.*;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.*;

@Service
public class ClassroomService {
    final int WEEKS = 20;

    @Resource
    private ClassroomMapper classroomMapper;
    @Resource
    private CourseInfoService courseInfoService;
    @Resource
    private CourseArrangementService courseArrangementService;
    @Resource
    private CourseSelectionService courseSelectionService;

    public int addClassroom(Map<String, Object> map) {
        Boolean bol= String.valueOf(map.get("classroom_mutil")).equals("true");
        Classroom classroom = new Classroom(String.valueOf(map.get("classroom_no")), bol, Integer.parseInt(map.get("classroom_capacity").toString()));
        return classroomMapper.insertSelective(classroom);
    }


    public List<Classroom> selectAllClassroom() {
        return classroomMapper.selectAll();
    }

    public PageResult<Classroom> selectAllClassroom(Map<String, Object> map) {
        Integer pageNum = Integer.valueOf(map.get("page").toString());
        Integer pageSize = Integer.valueOf(map.get("limit").toString());
        PageHelper.startPage(pageNum, pageSize);
        Example example = new Example(Classroom.class);
        List<Classroom> classrooms = classroomMapper.selectByExample(example);
        PageInfo<Classroom> userPageInfo = new PageInfo<Classroom>(classrooms);
        return new PageResult<Classroom>(userPageInfo.getTotal(), userPageInfo.getList());
    }

    /**
     * 教室分类
     *
     * @param list 查询到的教室表的信息
     * @return 分好类的map集合
     */
    private Map<String, List<Classroom>> classroomClassify(List<Classroom> list) {
        List<Classroom> bigneed = new ArrayList<>();
        List<Classroom> bigno = new ArrayList<>();
        List<Classroom> medineed = new ArrayList<>();
        List<Classroom> medino = new ArrayList<>();
        List<Classroom> smallneed = new ArrayList<>();
        List<Classroom> smallno = new ArrayList<>();
        for (Classroom classroom : list) {
            int people = classroom.getClassroomCapacity();
            boolean need = classroom.getClassroomMutil();
            if (people > 60) {
                if (need) bigneed.add(classroom);
                else bigno.add(classroom);
            } else {
                if (people > 30) {
                    if (need) medineed.add(classroom);
                    else medino.add(classroom);
                } else {
                    if (need) smallneed.add(classroom);
                    else smallno.add(classroom);
                }
            }
        }
        Map<String, List<Classroom>> map = new HashMap<>();
        map.put("bigneed", bigneed);
        map.put("bigno", bigno);
        map.put("medineed", medineed);
        map.put("medino", medino);
        map.put("smallneed", smallneed);
        map.put("smallno", smallno);
        return map;
    }

    /**
     * @param list 查询到的课程信息表的信息
     * @return 分好类的map集合
     */
    private Map<String, List<CourseArrangement>> courseClassify(List<CourseArrangement> list) {
        List<CourseArrangement> bigneed = new ArrayList<>();
        List<CourseArrangement> bigno = new ArrayList<>();
        List<CourseArrangement> medineed = new ArrayList<>();
        List<CourseArrangement> medino = new ArrayList<>();
        List<CourseArrangement> smallneed = new ArrayList<>();
        List<CourseArrangement> smallno = new ArrayList<>();
        for (CourseArrangement courseArrangement : list) {
            int people = courseSelectionService.getCourseSelectionByStudentNumber(courseArrangement.getCourseClassno()).size();
            boolean need = courseInfoService.getCourseByCourseNo(courseArrangement.getCourseNum()).getCourseMultimedia();
            if (people > 60) {
                if (need) bigneed.add(courseArrangement);
                else bigno.add(courseArrangement);
            } else {
                if (people > 30) {
                    if (need) medineed.add(courseArrangement);
                    else medino.add(courseArrangement);
                } else {
                    if (need) smallneed.add(courseArrangement);
                    else smallno.add(courseArrangement);
                }
            }
        }
        Map<String, List<CourseArrangement>> map = new HashMap<>();
        map.put("bigneed", bigneed);
        map.put("bigno", bigno);
        map.put("medineed", medineed);
        map.put("medino", medino);
        map.put("smallneed", smallneed);
        map.put("smallno", smallno);
        return map;
    }


    private Map<String, SetClassroomResult> setClassroomAndgetSurplus(Map<String, List<Classroom>> classroom, Map<String, List<CourseArrangement>> courseArrangement) {
        Map<String, SetClassroomResult> map = new HashMap<>();
        map.put("bigneed", courseArrangementService.setClassroomToArrangement(classroom.get("bigneed"), courseArrangement.get("bigneed")));
        map.put("mapbigno", courseArrangementService.setClassroomToArrangement(classroom.get("bigno"), courseArrangement.get("bigno")));
        map.put("medineed", courseArrangementService.setClassroomToArrangement(classroom.get("medineed"), courseArrangement.get("medineed")));
        map.put("medino", courseArrangementService.setClassroomToArrangement(classroom.get("medino"), courseArrangement.get("medino")));
        map.put("smallneed", courseArrangementService.setClassroomToArrangement(classroom.get("smallneed"), courseArrangement.get("smallneed")));
        map.put("smallno", courseArrangementService.setClassroomToArrangement(classroom.get("smallno"), courseArrangement.get("smallno")));
        return map;
    }

    public Map<String, SetClassroomResult> setClassroom() {
        List<Classroom> classroomList = selectAllClassroom();
        List<CourseArrangement> courseArrangementList = courseArrangementService.getCourseArrangementList();
        Map<String, List<Classroom>> classroomClassifys = classroomClassify(classroomList);
        Map<String, List<CourseArrangement>> courseArrangementClassifys = courseClassify(courseArrangementList);
        return setClassroomAndgetSurplus(classroomClassifys, courseArrangementClassifys);
    }

    public int delClassroom(Map<String, Object> map) {
        int result = classroomMapper.deleteByIds(map.get("id").toString());
        if (result == 0) {
            throw new MyException(ExceptionEnums.TABLE_SET_ERROR);
        }
        return result;
    }


    public int setClassroomInfo(Map<String, Object> map){
        String classroom_no=String.valueOf(map.get("classroom_no"));
        Boolean classroom_mutil=String.valueOf(map.get("classroom_mutil")).equals("true");;
        Integer classroom_capacity=Integer.parseInt(map.get("classroom_capacity").toString());
        Classroom classroom=new Classroom();
        Integer id=Integer.parseInt( map.get("id").toString());
        classroom.setId(id);
        classroom.setClassroomNo(classroom_no);
        classroom.setClassroomMutil(classroom_mutil);
        classroom.setClassroomCapacity(classroom_capacity);
        int result=classroomMapper.updateByPrimaryKeySelective(classroom);
        if(result==0){
            throw new MyException(ExceptionEnums.TABLE_SET_ERROR);
        }
        return result;
    }

    public void save(List<Classroom> list) {
        try {
            classroomMapper.insertList(list);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ExceptionEnums.TABLE_INFO_ERROR);
        }
    }
}
