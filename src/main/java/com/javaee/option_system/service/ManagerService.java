package com.javaee.option_system.service;

import com.javaee.option_system.entity.PageResult;
import com.javaee.option_system.entity.SetClassroomResult;
import com.javaee.option_system.entity.Term;
import com.javaee.option_system.enums.ExceptionEnums;
import com.javaee.option_system.exception.MyException;
import com.javaee.option_system.mapper.ManagersMapper;
import com.javaee.option_system.pojo.*;
import com.javaee.option_system.utils.JsonUtils;
import com.javaee.option_system.utils.JwtUtil;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.List;
import java.util.Map;

@Service
public class ManagerService {
    @Autowired
    private JwtUtil jwtUtil;
    @Resource
    private ManagersMapper managersMapper;
    @Resource
    private TeacherService teacherService;
    @Resource
    private StudentService studentService;
    @Resource
    private ClassroomService classroomService;
    @Resource
    private CourseArrangementService courseArrangementService;
    @Resource
    private CourseSelectionService courseSelectionService;
    @Resource
    private CourseInfoService courseInfoService;

    public PageResult<Students> managerSelectAllStu(Map<String, Object> map){
        return studentService.selectAllStu(map);
    }
    public PageResult<Teachers> managerSelectAllTea(Map<String, Object> map){
        return teacherService.selectAllTea(map);
    }
    public PageResult<Classroom> managerSelectAllClassroom(Map<String, Object> map){
        return classroomService.selectAllClassroom(map);
    }

    public int managerAddTeacher(Map<String, Object> map){
        return teacherService.addTeacher(map);
    }
    public int managerAddStudent(Map<String, Object> map){
        return studentService.addStudent(map);
    }
    public int managerAddClassroom(Map<String, Object> map){
        return classroomService.addClassroom(map);
    }


    public int resetStuPassword(Map<String, Object> map){
        return studentService.resetStuPassword(map);
    }

    public int delStu(Map<String, Object> map){
        return studentService.delStu(map);
    }
    public int delTea(Map<String, Object> map){
        return teacherService.delTea(map);
    }
    public int delClassroom(Map<String, Object> map){
        return classroomService.delClassroom(map);
    }

    public Managers selectManager(HttpServletRequest request){
        String token = request.getHeader("token");
        if (token==null || "".equals(token)) {
            throw new MyException(ExceptionEnums.NO_PERMISSIONS);
        }
        Claims claims ;
        try {
            claims = jwtUtil.parseJWT(token);
        } catch (Exception e) {
            throw new MyException(ExceptionEnums.TOKEN_TIME_OUT);
        }
        Example example = new Example(Managers.class);
        example.createCriteria().andEqualTo("managerNo",Integer.parseInt(claims.getId()));
        example.excludeProperties("managerPassword");
        List<Managers> managers = managersMapper.selectByExample(example);
        return managers.get(0);
    }

    public int setManagerInfo(Map<String, Object> map){
        String password=String.valueOf(map.get("manager_password"));
        String phone=String.valueOf(map.get("manager_phone"));
        Managers managers=new Managers();
        Integer id=Integer.parseInt( map.get("id").toString());
        managers.setId(id);
        managers.setManagerPassword(password);
        managers.setManagerPhone(phone);
        int result=managersMapper.updateByPrimaryKeySelective(managers);
        if(result==0){
            throw new MyException(ExceptionEnums.TABLE_SET_ERROR);
        }
        return result;
    }

    public int setClassroomInfo(Map<String, Object> map){
        return classroomService.setClassroomInfo(map);
    }

    @Value("${myconfig.filepath}")
    private File systemSetting;
    @Resource
    private JsonUtils jsonUtils;
    public int openNewTerm(Map<String, Object> map){
        courseArrangementService.createTable(map);
        courseSelectionService.createTable(map);
        courseInfoService.createTable(map);
        Term term=new Term(String.valueOf(map.get("year")),String.valueOf(map.get("term")));
        jsonUtils.writeJson(systemSetting,term);
        return 1;
    }

    public int fenpeiAganin(Map<String, Object> map){
        return courseArrangementService.fenpeiAgain(map);
    }

    public Map<String, SetClassroomResult> setClassroom(){
        return classroomService.setClassroom();
    }

}
