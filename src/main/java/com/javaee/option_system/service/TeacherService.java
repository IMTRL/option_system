package com.javaee.option_system.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javaee.option_system.entity.PageResult;
import com.javaee.option_system.enums.ExceptionEnums;
import com.javaee.option_system.exception.MyException;
import com.javaee.option_system.mapper.TeachersMapper;
import com.javaee.option_system.pojo.Managers;
import com.javaee.option_system.pojo.Teachers;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class TeacherService {
    @Resource
    private TeachersMapper teachersMapper;

    public List<Teachers> selectAllTea() {
        return teachersMapper.selectAll();
    }

    public int addTeacher(Map<String, Object> map) {
        Boolean bol= String.valueOf(map.get("teacher_onjob")).equals("true");
        Teachers teachers = new Teachers(String.valueOf(map.get("teacher_no")), String.valueOf(map.get("teacher_name")), String.valueOf(map.get("teacher_sex")), Integer.parseInt(map.get("teacher_age").toString()), String.valueOf(map.get("teacher_phone")), bol);
        return teachersMapper.insertSelective(teachers);
    }

    public PageResult<Teachers> selectAllTea(Map<String, Object> map) {
        Integer pageNum = Integer.valueOf(map.get("page").toString());
        Integer pageSize = Integer.valueOf(map.get("limit").toString());
        PageHelper.startPage(pageNum, pageSize);
        Example example = new Example(Teachers.class);
        example.excludeProperties("teacherPassword");
        List<Teachers> teachers = teachersMapper.selectByExample(example);
        PageInfo<Teachers> userPageInfo = new PageInfo<Teachers>(teachers);
        return new PageResult<Teachers>(userPageInfo.getTotal(), userPageInfo.getList());
    }

    public int delTea(Map<String, Object> map) {
        int result = teachersMapper.deleteByIds(map.get("id").toString());
        if (result == 0) {
            throw new MyException(ExceptionEnums.TABLE_SET_ERROR);
        }
        return result;
    }

    public void save(List<Teachers> list) {
        for (Teachers teachers : list) {
            teachers.setTeacherPassword("000000");
        }
        try {
            teachersMapper.insertList(list);
        } catch (Exception e) {
            e.printStackTrace();
            throw new MyException(ExceptionEnums.TABLE_INFO_ERROR);
        }
    }
}
