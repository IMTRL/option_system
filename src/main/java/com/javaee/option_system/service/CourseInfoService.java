package com.javaee.option_system.service;

import com.javaee.option_system.entity.Term;
import com.javaee.option_system.enums.ExceptionEnums;
import com.javaee.option_system.exception.MyException;
import com.javaee.option_system.mapper.CourseInfoMapper;
import com.javaee.option_system.pojo.CourseInfo;
import com.javaee.option_system.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.util.List;
import java.util.Map;

@Service
public class CourseInfoService {
    @Resource
    private CourseInfoMapper courseInfoMapper;
    @Value("${myconfig.filepath}")
    private File systemSetting;
    @Resource
    private JsonUtils jsonUtils;

    public List<CourseInfo> getCourseList(){
        return courseInfoMapper.selectAll();
    }

    public int createTable(Map<String, Object> map){
        String year=String.valueOf(map.get("year"));
        String term=String.valueOf(map.get("term"));
        int result=courseInfoMapper.createTable(year,term);
        /*if (result==0)
            throw new MyException(ExceptionEnums.TABLE_SET_ERROR);*/
        return result;
    }

    public CourseInfo getCourseByCourseNo(String courseNo){
        Term term=jsonUtils.readJson(systemSetting,Term.class);
        return courseInfoMapper.selectCourseByCourseNo(courseNo,term.getYear(),term.getTerm());
    }
}
