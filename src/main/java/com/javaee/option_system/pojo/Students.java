package com.javaee.option_system.pojo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Students {
    @Id
    @ExcelIgnore
    private Integer id;
    @ExcelProperty("学生编号")
    private String studentNo;
    @ExcelIgnore
    private String studentPassword;
    @ExcelProperty("学生姓名")
    private String studentName;
    @ExcelProperty("学生性别")
    private String studentSex;
    @ExcelProperty("学生年龄")
    private Integer studentAge;

    public Students(String studentNo, String studentName, String studentSex, Integer studentAge) {
        this.studentNo = studentNo;
        this.studentName = studentName;
        this.studentSex = studentSex;
        this.studentAge = studentAge;
    }
}
