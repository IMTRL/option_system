package com.javaee.option_system.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseSelection {
    @Id
    private Integer id;
    private String studentNumber;
    private String courseNumber;
    private Double studentScore;

}
