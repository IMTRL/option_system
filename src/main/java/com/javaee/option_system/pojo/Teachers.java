package com.javaee.option_system.pojo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Teachers {
    @Id
    @ExcelIgnore
    private Integer id;
    @ExcelProperty("教师编号")
    private String teacherNo;
    @ExcelIgnore
    private String teacherPassword;
    @ExcelProperty("教师姓名")
    private String teacherName;
    @ExcelProperty("教师性别")
    private String teacherSex;
    @ExcelProperty("教师年龄")
    private Integer teacherAge;
    @ExcelProperty("教师电话")
    private String teacherPhone;
    @ExcelProperty("教师在职状态")
    private Boolean teacherOnjob;

    public Teachers(String teacherNo, String teacherName, String teacherSex, Integer teacherAge, String teacherPhone, Boolean teacherOnjob) {
        this.teacherNo = teacherNo;
        this.teacherName = teacherName;
        this.teacherSex = teacherSex;
        this.teacherAge = teacherAge;
        this.teacherPhone = teacherPhone;
        this.teacherOnjob = teacherOnjob;
    }
}
