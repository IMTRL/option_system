package com.javaee.option_system.pojo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Managers {
  @Id
  @ExcelIgnore
  private Integer id;
  @ExcelProperty("管理员编号")
  private String managerNo;
  @ExcelIgnore
  private String managerPassword;
  @ExcelProperty("管理员姓名")
  private String managerName;
  @ExcelProperty("管理员性别")
  private String managerSex;
  @ExcelIgnore
  private Integer managerAge;
  @ExcelIgnore
  private String managerPhone;

}
