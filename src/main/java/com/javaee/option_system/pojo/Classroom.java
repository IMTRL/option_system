package com.javaee.option_system.pojo;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Classroom {
    @Id
    @ExcelIgnore
    private Integer id;
    @ExcelProperty("教室编号")
    private String classroomNo;
    @ExcelProperty("教室是否支持多媒体")
    private Boolean classroomMutil;
    @ExcelProperty("教室可容纳人数")
    private Integer classroomCapacity;

    public Classroom(String classroomNo, Boolean classroomMutil, Integer classroomCapacity) {
        this.classroomNo = classroomNo;
        this.classroomMutil = classroomMutil;
        this.classroomCapacity = classroomCapacity;
    }
}
