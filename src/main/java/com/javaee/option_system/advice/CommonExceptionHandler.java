package com.javaee.option_system.advice;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import com.javaee.option_system.entity.ExceptionResult;
import com.javaee.option_system.exception.MyException;

@ControllerAdvice
//拦截所有controller
public class CommonExceptionHandler {
    @ExceptionHandler(MyException.class)
    public ResponseEntity<ExceptionResult> handleException(MyException e){
        return ResponseEntity.ok(new ExceptionResult(e.getExceptionEnums()));
    }
}
